<?php

namespace App\Modules\Admin\Console;

use Illuminate\Console\Command;
use Illuminate\Console\GeneratorCommand;
use Illuminate\Support\Facades\Artisan;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class OneDragonMakeCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:modular {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new modular.';

    public function handle()
    {
        $name = $this->argument('name');

        // 自动生成模型
        Artisan::call('make:one-dragon-model', [
            'name' => $name
        ]);

        // 自动生成控制器
        Artisan::call('make:one-dragon-controller', [
            'name' => $name . 'Controller'
        ]);

        // 自动生成验证类
        Artisan::call('make:one-dragon-request', [
            'name' => $name . 'Request'
        ]);

        // 自动生成Service
        Artisan::call('make:one-dragon-service', [
            'name' => $name . 'Service'
        ]);

        $this->info('modular execution complete.');
    }
}
